__author__ = "Jakob Aungiers"
__copyright__ = "Jakob Aungiers 2018"
__version__ = "2.0.0"
__license__ = "MIT"

import os
import json
import time
import math
import matplotlib.pyplot as plt
from core.data_processor import DataLoader
from core.model import Model
import pandas as pd
import datetime

import sys

def plot_results(predicted_data, true_data):
	fig = plt.figure(facecolor='white')
	ax = fig.add_subplot(111)
	ax.plot(true_data, label='True Data')
	plt.plot(predicted_data, label='Prediction')
	plt.legend()
	plt.show()

def plot_results_multiple(predicted_data, true_data, prediction_len, stock_symbol):
	fig = plt.figure(facecolor='white')
	ax = fig.add_subplot(111)
	ax.plot(true_data, label='True Data')
	plt.xlabel("day")
	plt.ylabel("normalized price")
	plt.title(stock_symbol + " | Last %d days" % len(true_data)+" updated "+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	plt.grid(ls='--')
	#Pad the list of predictions to shift it in the graph to it's correct start
	# none = [None for p in range(500)] 
	# print('none: ', none)
	for i, data in enumerate(predicted_data):
		padding = [None for p in range(i * prediction_len)] 
		plt.plot(padding + data, label='Prediction')
		plt.legend(loc='upper left', frameon=True)
		# plt.legend()
	if not os.path.exists("predicted"): os.makedirs("predicted")
	plt.savefig("predicted/"+stock_symbol+".png", format='png', bbox_inches='tight', transparent=True)
	exit()
	# plt.show()

def run(stock_symbol, configs, model):

	try:
		print("stock_symbol", stock_symbol)
	except NameError:
		# print("load default stock_symbol",stock_symbol,"from config.json")
		stock_symbol = configs['data']['filename']

	# url = os.path.join('data', stock_symbol+".csv")
	url = 'http://bidschart.com/data_today/'+stock_symbol+'.csv'

	data = DataLoader(
		url,
		configs['data']['train_test_split'],
		configs['data']['columns'],
		configs['data']['sequence_length']
	)

	# model = Model()
	# model.build_model(configs)
	# model.load_model('saved_models/24102018-123143-e1.h5')
	# x, y = data.get_train_data(
	# 	seq_len = configs['data']['sequence_length'],
	# 	normalise = configs['data']['normalise']
	# )

	# in-memory training
	# model.train(
	# 	x,
	# 	y,
	# 	epochs = configs['training']['epochs'],
	# 	batch_size = configs['training']['batch_size'],
	# 	save_dir = configs['model']['save_dir']
	# )

	# out-of memory generative training
	# steps_per_epoch = math.ceil((data.len_train - configs['data']['sequence_length']) / configs['training']['batch_size'])
	# model.train_generator(
	# 	data_gen = data.generate_train_batch(
	# 		seq_len = configs['data']['sequence_length'],
	# 		batch_size = configs['training']['batch_size'],
	# 		normalise = configs['data']['normalise']
	# 	),
	# 	epochs = configs['training']['epochs'],
	# 	batch_size = configs['training']['batch_size'],
	# 	steps_per_epoch = steps_per_epoch,
	# 	save_dir = configs['model']['save_dir']
	# )
	
	x_test, y_test = data.get_test_data(
		seq_len = configs['data']['sequence_length'],
		normalise = configs['data']['normalise']
	)

	predictions = model.predict_sequences_multiple(x_test, configs['data']['sequence_length'], configs['data']['sequence_length'])
	#predictions = model.predict_sequence_full(x_test, configs['data']['sequence_length'])
	# predictions = model.predict_point_by_point(x_test)        

	# pos_padding = len(y_test)%configs['data']['sequence_length']
	num_days = 300

	# plot_results_multiple(predictions, y_test, configs['data']['sequence_length'])
	plot_results_multiple(
		predictions[-int(num_days/configs['data']['sequence_length']):], 
		y_test[-(num_days):], 
		configs['data']['sequence_length'],
		stock_symbol)
	# plot_results(predictions, y_test)

def main():

	set_symbols = pd.read_csv("data/SET_symbols.csv")
	set_symbols.sort_values('Symbol', ascending=False, inplace=True)
	stock_symbols = set_symbols['Symbol'].unique().tolist()
	print("Loaded %d stock symbols" % len(stock_symbols))

	configs = json.load(open('config.json', 'r'))
	if not os.path.exists(configs['model']['save_dir']): os.makedirs(configs['model']['save_dir'])

	model = Model()
	model.load_model('saved_models/07112018-170256-e2.h5')

	for symbol in stock_symbols:

		# print("predict stock", symbol)
		# run(symbol, configs, model)

		try:
			run(symbol, configs, model)
		except Exception as e:
			if(e.code == 404):
				print("Stock Data not found")
			else:
				print("Unknow error", e)
		
	exit()

if __name__=='__main__':
	main()